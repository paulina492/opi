package utils;

import org.openqa.selenium.By;

public class Field {
    private FieldType fieldType;
    private By locator;
    private String xpathPattern;
    private String[] frames;

    public Field(FieldType fieldType, By locator, String... frames) {
        this.fieldType = fieldType;
        this.locator = locator;
        this.frames = frames;
    }

    public Field(FieldType fieldType, String xpathPattern, String... frames) {
        this.fieldType = fieldType;
        this.xpathPattern = xpathPattern;
        this.frames = frames;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public By getLocator() {
        return locator;
    }

    public String getXpathPattern() {
        return xpathPattern;
    }

    public String[] getFrames() {
        return frames;
    }
}
