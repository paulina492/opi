package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

import static utils.WebDriverProvider.getDriver;

public class Page {
    protected Map<String, Field> fields = new HashMap();

    protected void addField(Enum<?> fieldName, Field field) {
        if (fields.containsKey(fieldName.toString())) {
            throw new IllegalArgumentException("Field with a given name is already defined on the page: " + fieldName);
        }

        fields.put(fieldName.toString(), field);
    }

    public WebDriverWait getDefaultWebDriverWait() {
        return new WebDriverWait(getDriver(), 30);
    }

    public void waitForPageLoading() {

        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        wait.until(pageLoadCondition);
    }

    public Field getField(String fieldName) {
        if (fields.containsKey(fieldName)) {
            return fields.get(fieldName);

        } else {
            throw new RuntimeException("Field " + fieldName + " not defined on page.");
        }
    }

    protected void switchToFrames(String... frames) {
        getDriver().switchTo().defaultContent();

        WebDriverWait wait = getDefaultWebDriverWait();
        for (String frame : frames) {
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
            waitForPageLoading();
        }
    }

    public void click(String fieldName) {
        Field field = getField(fieldName);
        switchToFrames(field.getFrames());

        click(field);
        waitForPageLoading();
    }

    public void click(Field field) {

        switchToFrames(field.getFrames());

        new Actions(getDriver()).moveToElement(findElementRetry(getDriver(), field.getLocator())).click().build().perform();

        waitForPageLoading();
    }

    protected WebElement findElementRetry(WebDriver driver, By by) {

        WebElement ret = driver.findElement(by);
        return ret;
    }

    public void setFieldValue(String fieldName, String value) {
        Field field = getField(fieldName);
        setFieldValue(field, value);
    }

    public void setFieldValue(Field field, String value) {
        if (value == null || value.isEmpty()) {
            return;
        }

        switch (field.getFieldType()) {

            case TEXT:
                setText(field, value);
                break;

            default:
                throw new RuntimeException("Field type not supported: " + field.getFieldType());
        }
    }

    protected void setText(Field field, String value) {
        switchToFrames(field.getFrames());

        WebElement weField = (WebElement) field.getLocator();
        setText(weField, value);
    }

    protected void setText(WebElement weField, String value) {
        waitForPageLoading();
        weField.clear();
        new Actions(getDriver()).moveToElement(weField).click().build().perform();

        waitForPageLoading();
    }

    public String getText(String fieldName) {
        Field field = getField(fieldName);
        String value = "";

        WebElement webElement = getDriver().findElement(field.getLocator());

        value = webElement.getText();

        return value;
    }
}
