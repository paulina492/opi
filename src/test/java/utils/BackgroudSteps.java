package utils;

import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.text.ParseException;

public class BackgroudSteps {

    @Before
    public void before() throws ParseException {

        Buffer.initialize();
    }

    @After
    public void after(){
        WebDriverProvider.quit();
    }
}
