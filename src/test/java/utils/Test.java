package utils;

import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static List<Integer> listTest(List<Integer> list){
        return list.stream()
                .filter(a -> a%2 == 0)
                .collect(Collectors.toList());
    }
 }
