package utils;

import java.util.HashMap;
import java.util.Map;

import static com.sun.jna.platform.win32.COM.tlb.TlbImp.logInfo;

public class Buffer {

    private static final ThreadLocal<Map<String, String>> buffer = new ThreadLocal<Map<String, String>>() {
        @Override
        protected Map<String, String> initialValue() {
            return new HashMap<>();
        }
    };

    public static Map<String, String> get() {
        return buffer.get();
    }

    public static void initialize() {
        get().clear();
        logInfo("Buffer.init");
    }

    public static void set(String key, String value) {
        get().put(key, value);
        logInfo(String.format("Buffer.set: %s=%s", key, value));
    }

    public static String get(String key) {
        logInfo(String.format("Buffer.get: %s=%s", key, get().get(key)));
        return get().get(key);
    }
}

