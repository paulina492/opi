package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverProvider {
    public static final Integer IMPLICITLY_LOAD_TIME = 0;
    public static final Integer SCRIPT_LOAD_TIME = 15;
    public static final Integer PAGE_LOAD_TIME = 60;

    private static WebDriver driver = new ChromeDriver();

    public static WebDriver getDriver() {
        return driver;
    }

    public static void initialize() {

        String driverPath = "src/test/resources/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);

        getDriver().manage().deleteAllCookies();
        getDriver().manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIME, TimeUnit.SECONDS);
        getDriver().manage().timeouts().setScriptTimeout(SCRIPT_LOAD_TIME, TimeUnit.SECONDS);
        setDriverImplicitWaitTimeToDefault();
    }

    public static void waitForLoad() {
        getDriver().manage().deleteAllCookies();
        getDriver().manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIME, TimeUnit.SECONDS);
        getDriver().manage().timeouts().setScriptTimeout(SCRIPT_LOAD_TIME, TimeUnit.SECONDS);
        setDriverImplicitWaitTimeToDefault();
    }

    private static ChromeDriver createWebDriver(final String browser) {

        String driverPath = "src/test/resources/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);

        return new ChromeDriver();
    }

    public static void quit() {
        getDriver().quit();
    }

    public static void setDriverImplicitWaitTimeToDefault() {
        getDriver().manage().timeouts().implicitlyWait(WebDriverProvider.IMPLICITLY_LOAD_TIME, TimeUnit.SECONDS);
    }
}