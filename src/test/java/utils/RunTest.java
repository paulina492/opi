package utils;

import java.util.ArrayList;
import java.util.List;

public class RunTest {
    public static void main(String[] args) {

        List<Integer> test1 = new ArrayList<>();
        List<Integer> test2 = new ArrayList<>();
        test2.add(0);
        test2.add(1);
        test2.add(2);
        test2.add(3);
        test2.add(4);

        System.out.println("Test1: "+Test.listTest(test1));
        System.out.println("Test2: "+Test.listTest(test2));

    }
}
