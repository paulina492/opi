package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {

    private static Properties properties;

    static {
        properties = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = Configuration.class.getClassLoader().getResourceAsStream("config.properties");
            properties.load(inputStream);

        } catch (FileNotFoundException e) {
            System.out.println("Config file not found: " + inputStream);
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getParameterValue(String name) {
        return properties.getProperty(name);
    }
}
