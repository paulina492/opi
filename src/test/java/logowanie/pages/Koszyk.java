package logowanie.pages;

import org.openqa.selenium.By;
import utils.Field;
import utils.FieldType;
import utils.Page;
import utils.PageName;

@PageName("Koszyk")
public class Koszyk extends Page {

    public enum FieldName {
        NAZWA,
        CENA
    }

    public Koszyk() {
        addField(FieldName.NAZWA, new Field(FieldType.TEXT_MESSAGE, By.xpath("localizator")));
        addField(FieldName.CENA, new Field(FieldType.TEXT_MESSAGE, By.xpath("localizator")));
    }
}
