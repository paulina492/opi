package logowanie.pages;

import org.openqa.selenium.By;
import utils.Field;
import utils.FieldType;
import utils.Page;
import utils.PageName;

@PageName("Tshirts")
public class Tshirts extends Page {

    public enum FieldName {
        NAZWA,
        CENA,
        DODAJ_DO_KOSZYKA,
        ZAMKNIJ,
        KOSZYK
    }

    public Tshirts() {
        addField(FieldName.NAZWA, new Field(FieldType.TEXT_MESSAGE, By.xpath("//h1[@itemprop='name']")));
        addField(FieldName.CENA, new Field(FieldType.TEXT_MESSAGE, By.xpath("//span[@id='our_price_display']")));
        addField(FieldName.DODAJ_DO_KOSZYKA, new Field(FieldType.BUTTON, By.partialLinkText("Add to cart")));
        addField(FieldName.ZAMKNIJ, new Field(FieldType.BUTTON, By.xpath("//span[@title='Close window']")));
        addField(FieldName.KOSZYK, new Field(FieldType.BUTTON, By.xpath("//a[@title='View my shopping cart']")));
    }
}
