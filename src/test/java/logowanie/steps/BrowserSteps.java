package logowanie.steps;

import utils.WebDriverProvider;

import static utils.WebDriverProvider.getDriver;


public class BrowserSteps {
    public static void openUrl(String pageName) {

        String driverPath = "src/test/resources/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);
        getDriver().manage().window().maximize();
        getDriver().get(pageName);

        WebDriverProvider.waitForLoad();
    }
}
