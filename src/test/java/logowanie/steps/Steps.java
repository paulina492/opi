package logowanie.steps;

import cucumber.api.java.pl.Wtedy;
import cucumber.api.java.pl.Zakładając;
import logowanie.pages.Koszyk;
import logowanie.pages.Tshirts;
import org.junit.Assert;
import org.openqa.selenium.UnhandledAlertException;
import utils.Buffer;

public class Steps {
    Tshirts tshirts = new Tshirts();
    Koszyk koszyk = new Koszyk();

    @Zakładając("\\[Tshirts] otworzę stronę logowania(.*)")
    public void openPage(String pageName) {
        BrowserSteps.openUrl(pageName);
    }

    @Wtedy("\\[Tshirts] w polu (.*) wprowadzę (.*)")
    public void setText(String fieldName, String value) {
        tshirts.setFieldValue(fieldName, value);
    }

    @Wtedy("\\[Tshirts] kliknę (.*)")
    public void click(String fieldName) {
        try {
            tshirts.click(fieldName);
        } catch (UnhandledAlertException e) {
        }
    }

    @Wtedy("\\[Tshirts] zapamiętam (.*)")
    public void rememberedErrorMessage(String fieldName) {
        Buffer.set(fieldName, tshirts.getText(fieldName));
        System.out.println("BUFFER: " + Buffer.get(fieldName));
    }

    @Wtedy("\\[Tshirts] zawiera:$")
    public void errorMessage(String expectedMessage) {
        String actualDisplayedMessages = Buffer.get("ERROR");

        Assert.assertTrue("Komunikat błędu nie jest prawidłowy", actualDisplayedMessages.equals(expectedMessage));
    }
}
