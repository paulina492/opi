# language: pl
@test

Funkcja: TEST_01_Dodawanie_produktu_do_koszyka

  Streszczenie
  Otwieram stronę do zakupu produktu
  Dodaję produkt do koszyka
  Sprawdzam w koszyku poprawność dodanych danych


  Szablon scenariusza: TEST_01_Dodawanie_produktu_do_koszyka

    Zakładając [Tshirts] otworzę stronę logowania <STRONA>
    Wtedy [Tshirts] zapamiętam NAZWA
    Wtedy [Tshirts] zapamiętam CENA
#    Wtedy [Tshirts] kliknę DODAJ_DO_KOSZYKA
#    Wtedy [Tshirts] kliknę ZAMKNIJ
#    I [Tshirts] kliknę KOSZYK
#    Wtedy [Cart] sprawdzam czy NAZWA jest poprawna
#    Wtedy [Cart] sprawdzam czy CENA jest poprawna


    Przykłady:
      | STRONA                                                                  |
      | http://automationpractice.com/index.php?id_product=3&controller=product |